Use the following credentials for initial setup:

```
username: root
email: root@cloudron.local
password: changeme123
```

To change the root account email, follow the [docs](https://cloudron.io/documentation/apps/discourse/#changing-root-account-email).

**Please change the admin password and email on first login**

