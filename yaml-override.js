#!/usr/bin/env node

'use strict';

var fs = require('fs'),
    lodash = require('lodash'),
    yaml = require('js-yaml');

var target = yaml.safeLoad(fs.readFileSync(process.argv[2] ,'utf8'));
var source = yaml.safeLoad(fs.readFileSync(process.argv[3], 'utf8'));
target = lodash.merge(target, source);
fs.writeFileSync(process.argv[2], yaml.safeDump(target));

