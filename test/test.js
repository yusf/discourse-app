#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    var email, token;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function waitForUrl(url, done) {
        browser.wait(function () {
            return browser.getCurrentUrl().then(function (currentUrl) {
                return currentUrl === url;
            });
        }, TIMEOUT).then(function () { done(); });
    }

    function login(username, password, done) {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn + '/login');
        }).then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.id('login-account-name'))), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.id('login-account-name')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.id('login-account-password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//button[@aria-label="Log In"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(text(), "setup wizard")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function clearNotification(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.findElement(by.xpath('//a[@href="/u/root"]')).click();
        }).then(function () {
            return browser.sleep(3000);
        }).then(function () {
            done();
        });
    }

    function checkDashboard(done) {
        browser.get('https://' + app.fqdn + '/admin').then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(@href, "/admin/badges")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function checkEmail(done) {
        browser.get('https://' + app.fqdn + '/admin/email').then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//input[contains(@placeholder, "email address to test")]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//input[contains(@placeholder, "email address to test")]')).sendKeys('test@cloudron.io');
        }).then(function () {
            return browser.findElement(by.xpath('//button[@aria-label="Send Test Email"]')).click();
        }).then(function () {
            return browser.sleep(3000);
        }).then(function () {
            return browser.get('https://' + app.fqdn + '/admin/email/sent');
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//td[contains(text(), "test_message")]')), TIMEOUT);
        }).then(function() {
            done();
        });
    }

    // we choose calendar plugin since it also has 'gem' in plugins.rb
    function installPlugin(done) {
        let out = execSync(`cloudron exec --app ${app.id} -- git clone https://github.com/discourse/discourse-calendar /app/code/plugins/discourse-calendar`);
        out = execSync(`cloudron exec --app ${app.id} -- git -C /app/code/plugins/discourse-calendar reset --hard e6d4357cf2a4353dc1c9abb4256cd5b8ff09bf4d`);
        console.log('installing gems of plugin');
        out = execSync(`cloudron exec --app ${app.id} -- bundle exec rake plugin:install_gems['discourse-calendar']`);
        console.log(out.toString('utf8'));
        if (out.toString('utf8').indexOf('ERROR') !== -1) return done(new Error('error installing plugin gems'));
        console.log('restarting app');
        execSync(`cloudron restart --app ${app.id}`);
        done();
    }

    function checkPlugin(done) {
        browser.get('https://' + app.fqdn + '/admin/plugins').then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(text(), "discourse-calendar")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function uploadImage(done) {
        browser.get('https://' + app.fqdn + '/u/root/preferences/account');
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can login', login.bind(null, 'root', 'changeme123'));
    it('can clear notification', clearNotification);
    it('can check admin dashboard', checkDashboard);
    it('can install plugin', installPlugin);
    it('can check plugin', checkPlugin);
    it('can see mail', checkEmail);

    it('can restart app', function (done) {
        execSync('cloudron restart');
        done();
    });
    it('can see admin dashboard', checkDashboard);
    it('can check plugin', checkPlugin);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can see admin dashboard', checkDashboard);
    it('can check plugin', checkPlugin);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login.bind(null, 'root', 'changeme123'));
    it('can see admin dashboard', checkDashboard);
    it('can check plugin', checkPlugin);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id org.discourse.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can login', login.bind(null, 'root', 'changeme123'));
    it('can see admin dashboard', checkDashboard);
    it('can install plugin', installPlugin);
    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can see admin dashboard', checkDashboard);
    it('can check email', checkEmail);
    it('can check plugin', checkPlugin);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
