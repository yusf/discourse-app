FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

ARG VERSION=2.4.1

RUN mkdir -p /app/pkg /app/code
WORKDIR /app/code

RUN apt-get update && \
    apt-get install -y make gcc g++ libpq-dev zlib1g-dev rbenv brotli advancecomp jhead jpegoptim libjpeg-turbo-progs optipng pngquant gifsicle && \
    rm -r /var/cache/apt /var/lib/apt/lists

# asset compilation requires node 10
RUN mkdir -p /usr/local/node-10.16.0 && \
    curl -L https://nodejs.org/dist/v10.16.0/node-v10.16.0-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-10.16.0

ENV PATH /usr/local/node-10.16.0/bin:$PATH

# note that changing ruby version means we have to recompile plugins
RUN echo 'gem: --no-document' >> /usr/local/etc/gemrc && \
    git clone https://github.com/sstephenson/ruby-build.git && \
    cd ruby-build && ./install.sh && cd .. && rm -rf ruby-build && \
    (ruby-build 2.6.3 /usr/local)

RUN gem install bundler -v 2.1.1

ENV RAILS_ENV production
RUN curl -L https://github.com/discourse/discourse/archive/v${VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    bundle install --deployment --without test development && \
    chown -R cloudron:cloudron /app/code

# svgo required for image optimization
RUN npm install -g svgo

# for yaml-override
RUN cd /app/pkg && npm install js-yaml@3.13.1 lodash@4.17.15

# the code detects these values from a git repo otherwise
RUN echo "\$git_version='${VERSION}'\n\$full_version='${VERSION}'\n\$last_commit_date=DateTime.strptime('$(date +%s)','%s')" > /app/code/config/version.rb

# -0 sets the separator to null (instead of newline). /s will make '.' match newline. .*? means non-greedy
RUN mv /app/code/config/site_settings.yml /app/code/config/site_settings.yml.default && \
    ln -s /run/discourse/site_settings.yml /app/code/config/site_settings.yml && \
    perl -i -0p -e 's/force_https:.*?default: false/force_https:\n    default: true/ms;' /app/code/config/site_settings.yml.default

# public/ - directory has files that don't change
#   assets/ - generated assets
# plugins/ - plugin code
# app/assets - app assets
#   javascripts/ - for some reason a plugin on activate will write into plugins/ (839916aa490 in discourse)
RUN ln -sf /run/discourse/discourse.conf /app/code/config/discourse.conf && \
    rm -rf /app/code/log && ln -sf /run/discourse/log /app/code/log && \
    mv /app/code/public /app/code/public.original && ln -s /run/discourse/public /app/code/public && \
    mv /app/code/plugins /app/code/plugins.original && ln -s /app/data/plugins /app/code/plugins && \
    ln -s /run/discourse/assets_js_plugins /app/code/app/assets/javascripts/plugins && \
    ln -s /run/discourse/tmp /app/code/tmp && \
    rm -rf /root/.gem && ln -s /tmp/gemcache /root/.gem && chown -R cloudron:cloudron /root/.gem

# add nginx config
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
ADD nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
RUN ln -s /etc/nginx/sites-available/discourse /etc/nginx/sites-enabled/discourse
RUN cp /app/code/config/nginx.sample.conf /etc/nginx/sites-available/discourse
# https://perldoc.perl.org/perlre.html#Modifiers
RUN perl -i -0p -e 's/upstream discourse \{.*?\}/upstream discourse { server 127.0.0.1:3000; }/ms;' \
    -e 's,.*access_log.*,access_log /dev/stdout;,;' \
    -e 's,.*error_log.*,error_log /dev/stderr info;,;' \
    -e 's/server_name.*/server_name _;/g;' \
    -e 's,/var/www/discourse,/app/code,g;' \
    -e 's,/var/nginx,/run/nginx,g;' \
    -e 's,brotli_static on,# brotli_static on,g;' \
    /etc/nginx/sites-available/discourse

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/discourse/supervisord.log /var/log/supervisor/supervisord.log

COPY maxmind/*.mmdb /app/code/vendor/data/
COPY start.sh yaml-override.js /app/pkg/

CMD [ "/app/pkg/start.sh" ]
